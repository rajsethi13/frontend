export const NEW_ID = 'new';
export const DURATION = 5000;
export const UNDO_DURATION = 10000;
export const THIRTY = 30;
export const FORM = 'form';
export const LIST = 'list';
