import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { switchMap } from 'rxjs';
import { TokenService } from '../../auth/token/token.service';
import {
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
} from '../../auth/token/constants';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UpdatePhoneService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  addUnverifiedPhone(unverifiedPhone: string) {
    return this.token.getToken().pipe(
      switchMap(token => {
        const url =
          this.token.appBasePath + environment.routes.ADD_UNVERIFIED_PHONE;
        const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set(AUTHORIZATION, BEARER_HEADER_PREFIX + token);
        return this.http.post(url, { unverifiedPhone }, { headers });
      }),
    );
  }

  verifyPhone(otp: string) {
    return this.token.getToken().pipe(
      switchMap(token => {
        const url = this.token.appBasePath + environment.routes.VERIFY_PHONE;
        const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set(AUTHORIZATION, BEARER_HEADER_PREFIX + token);
        return this.http.post(url, { otp }, { headers });
      }),
    );
  }
}
