import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TokenService } from '../../auth/token/token.service';
import { switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
  ) {}

  findModels(
    model: string,
    filter = '',
    sortOrder = 'asc',
    pageNumber = 0,
    pageSize = 10,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `/api/${model}/v1/list`;
        const params = new HttpParams()
          .set('limit', pageSize.toString())
          .set('offset', (pageNumber * pageSize).toString())
          .set('search', filter)
          .set('sort', sortOrder);

        return this.http.get(url, { params, headers });
      }),
    );
  }
}
