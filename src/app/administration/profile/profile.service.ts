import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { throwError, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CLOSE, CURRENT_PASSWORD_MISMATCH } from '../../constants/messages';
import { DURATION } from '../../constants/app-constants';
import { TokenService } from '../../auth/token/token.service';
import { StorageService } from '../../auth/storage/storage.service';
import { LOGGED_IN } from '../../auth/token/constants';
import { environment } from '../../../environments/environment';
import {
  CLIENT_ID,
  ISSUER_URL,
  LOGIN_URL,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  USER_UUID,
} from '../../constants/storage';

@Injectable()
export class ProfileService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private store: StorageService,
    private snackBar: MatSnackBar,
  ) {}

  updateOpenIDClaims(
    given_name: string,
    middle_name: string,
    family_name: string,
    nickname: string,
    gender: string,
    birthdate: string,
    website: string,
    zoneinfo: string,
    locale: string,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl + environment.routes.UPDATE_OPENID_CLAIMS_ENDPOINT,
          {
            given_name,
            middle_name,
            family_name,
            nickname,
            gender,
            birthdate,
            website,
            zoneinfo,
            locale,
          },
          {
            headers,
          },
        );
      }),
    );
  }

  disablePasswordLess() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl +
          environment.routes.DISABLE_PASSWORD_LESS_LOGIN_ENDPOINT;
        return this.http.post(
          url,
          {},
          {
            headers,
          },
        );
      }),
    );
  }

  enablePasswordLess() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl +
          environment.routes.ENABLE_PASSWORD_LESS_LOGIN_ENDPOINT;
        return this.http.post(
          url,
          {},
          {
            headers,
          },
        );
      }),
    );
  }

  getAuthServerUser() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + environment.routes.GET_AUTH_SERVER_USER;
        return this.http.get(url, {
          headers,
        });
      }),
    );
  }

  getOIDCProfile() {
    return this.token.loadProfile();
  }

  uploadAvatar(selectedFile) {
    const uploadData = new FormData();
    uploadData.append('file', selectedFile, selectedFile.name);
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl + environment.routes.AVATAR_UPLOAD_ENDPOINT,
          uploadData,
          {
            reportProgress: true,
            headers,
          },
        );
      }),
    );
  }

  setAuthServerUser(user) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl + environment.routes.SET_AUTH_SERVER_USER,
          user,
          {
            headers,
          },
        );
      }),
    );
  }

  changePassword(
    currentPassword: string,
    newPassword: string,
    repeatPassword: string,
  ) {
    if (newPassword !== repeatPassword) {
      this.snackBar.open(CURRENT_PASSWORD_MISMATCH, CLOSE, {
        duration: DURATION,
      });
      return throwError(() => ({ message: CURRENT_PASSWORD_MISMATCH }));
    } else {
      return this.token.getConfigAndHeaders().pipe(
        switchMap(({ config, headers }) => {
          return this.http.post(
            config.issuerUrl + environment.routes.CHANGE_PASSWORD_ENDPOINT,
            {
              currentPassword,
              newPassword,
            },
            {
              headers,
            },
          );
        }),
      );
    }
  }

  logout() {
    this.clearInfoStorage();
  }

  deleteAvatar() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl + environment.routes.DELETE_AVATAR_ENDPOINT,
          {},
          {
            headers,
          },
        );
      }),
    );
  }

  deleteUser() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl + environment.routes.DELETE_ME_ENDPOINT,
          {},
          {
            headers,
          },
        );
      }),
    );
  }

  setPassword() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.token.loadProfile().pipe(
          switchMap(userInfo => {
            return this.http.post(
              config.issuerUrl + environment.routes.FORGOT_PASSWORD,
              { emailOrPhone: userInfo.email || userInfo.phone_number },
            );
          }),
        );
      }),
    );
  }

  checkServerForPhoneRegistration() {
    return this.token.config;
  }

  emailVerificationCode() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl +
            environment.routes.EMAIL_VERIFICATION_CODE_ENDPOINT,
          undefined,
          {
            headers,
          },
        );
      }),
    );
  }

  updateEmail(email: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.post(
          config.issuerUrl + environment.routes.ADD_UNVERIFIED_EMAIL_ENDPOINT,
          { unverifiedEmail: email },
          {
            headers,
          },
        );
      }),
    );
  }

  setAutoLoginInDuration() {
    timer(DURATION).subscribe(() => {
      const loggedIn = this.store.getItem(LOGGED_IN);
      if (loggedIn !== 'true') {
        this.token.logIn();
      }
    });
  }

  clearInfoStorage() {
    this.store.removeItem(CLIENT_ID);
    this.store.removeItem(REDIRECT_URI);
    this.store.removeItem(SILENT_REFRESH_REDIRECT_URI);
    this.store.removeItem(LOGIN_URL);
    this.store.removeItem(ISSUER_URL);
    this.store.removeItem(USER_UUID);
  }

  getAvatar() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        return this.http.get(config.profileURL, { headers });
      }),
    );
  }
}
