import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';

import { OAuth2ProviderService } from './oauth2-provider.service';
import { StorageService } from '../../auth/storage/storage.service';
import { TokenService } from '../../auth/token/token.service';

describe('OAuth2ProviderService', () => {
  const token: Partial<TokenService> = {
    getToken: () => of('access_token'),
    getHeaders: () => of(new HttpHeaders()),
    logOut() {},
  };
  const store: Partial<StorageService> = {
    getItem: () => 'item',
  };

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: TokenService, useValue: token },
        { provide: StorageService, useValue: store },
      ],
    }),
  );

  it('should be created', () => {
    const service: OAuth2ProviderService = TestBed.get(OAuth2ProviderService);
    expect(service).toBeTruthy();
  });
});
