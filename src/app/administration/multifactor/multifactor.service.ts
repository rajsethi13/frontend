import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../../auth/token/token.service';
import { forkJoin, switchMap } from 'rxjs';
import {
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
} from '../../auth/token/constants';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MultifactorService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
  ) {}

  enable2fa() {
    return forkJoin({
      config: this.token.config,
      token: this.token.getToken(),
    }).pipe(
      switchMap(({ config, token }) => {
        return this.http.post(
          config.issuerUrl +
            environment.routes.INITIALIZE_2FA +
            '?restart=true',
          null,
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  verify2fa(otp: string) {
    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.post(
          this.token.appBasePath + environment.routes.VERIFY_2FA,
          { otp },
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }

  disable2fa() {
    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.post(
          this.token.appBasePath + environment.routes.DISABLE_2FA,
          null,
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          },
        );
      }),
    );
  }
}
