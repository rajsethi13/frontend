import { Injectable } from '@angular/core';
import { STORAGE } from '../../constants/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { REMOVE_STORAGE_ENDPOINT } from '../../constants/url-paths';
import { TokenService } from '../../auth/token/token.service';
import { Observable, switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CloudStorageService {
  headers: Observable<HttpHeaders> = this.token.getHeaders();
  model = STORAGE;
  hideAccessKey = true;
  hideSecretKey = true;

  constructor(
    private readonly token: TokenService,
    private readonly http: HttpClient,
  ) {}

  getCloud(uuid) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl =
          config.issuerUrl + '/api/' + this.model + '/v1/get/' + uuid;
        return this.http.get(requestUrl, { headers });
      }),
    );
  }

  updateCloudStorage(
    uuid: string,
    version: string,
    name: string,
    region: string,
    endpoint: string,
    accessKey: string,
    secretKey: string,
    bucket: string,
    basePath: string,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl =
          config.issuerUrl + '/api/' + this.model + '/v1/modify/' + uuid;

        const updateCloudStorageCredentials = {
          version,
          name,
          region,
          endpoint,
          accessKey,
          secretKey,
          bucket,
          basePath,
        };

        return this.http.post(requestUrl, updateCloudStorageCredentials, {
          headers,
        });
      }),
    );
  }

  createCloudStorage(
    version: string,
    name: string,
    region: string,
    endpoint: string,
    accessKey: string,
    secretKey: string,
    bucket: string,
    basePath: string,
  ) {
    const cloudCredentials = {
      version,
      name,
      region,
      endpoint,
      accessKey,
      secretKey,
      bucket,
      basePath,
    };
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + '/api/' + this.model + '/v1/add';
        return this.http.post(requestUrl, cloudCredentials, { headers });
      }),
    );
  }

  deleteStorage(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = `${config.issuerUrl}${REMOVE_STORAGE_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
