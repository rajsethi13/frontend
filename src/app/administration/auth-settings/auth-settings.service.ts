import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap, map } from 'rxjs/operators';
import { OpenIDConfiguration } from '../../interfaces/open-id-configuration.interface';
import { IAuthSettings } from './auth-settings.interface';
import {
  DELETE_BEARER_TOKENS_ENDPOINT,
  DELETE_USER_SESSIONS_ENDPOINT,
  GET_SETTINGS_ENDPOINT,
  GET_TRUSTED_CLIENTS_ENDPOINT,
  LIST_EMAILS_ENDPOINT,
  LIST_STORAGE_ENDPOINT,
  LOGOUT_URL,
  OPENID_CONFIGURATON_ENDPOINT,
  UPDATE_SETTINGS_ENDPOINT,
} from '../../constants/url-paths';
import { TokenService } from '../../auth/token/token.service';
import { StorageService } from '../../auth/storage/storage.service';
import { environment } from '../../../environments/environment';
import { ListResponse } from '../listing/listing-datasource';

@Injectable({
  providedIn: 'root',
})
export class AuthSettingsService {
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private store: StorageService,
  ) {}

  getSettings() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + GET_SETTINGS_ENDPOINT;
        return this.http.get(requestUrl, { headers });
      }),
    );
  }

  getClientSettings<T>(appURL: string) {
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        const requestUrl = appURL + GET_SETTINGS_ENDPOINT;
        return this.http.get<T>(requestUrl, { headers });
      }),
    );
  }

  updateClientSettings(appURL: string, payload: IAuthSettings) {
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        const requestUrl = appURL + UPDATE_SETTINGS_ENDPOINT;
        return this.http.post(requestUrl, payload, { headers });
      }),
    );
  }

  getClientList() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + GET_TRUSTED_CLIENTS_ENDPOINT;
        return this.http.get(requestUrl, { headers });
      }),
    );
  }

  getStorageList() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + LIST_STORAGE_ENDPOINT;
        return this.http.get<{ docs: any[] }>(requestUrl, { headers });
      }),
      map(data => data.docs),
    );
  }

  update(
    issuerUrl: string,
    disableSignup: boolean,
    infrastructureConsoleClientId: string,
    enableChoosingAccount: boolean,
    refreshTokenExpiresInDays: number,
    authCodeExpiresInMinutes: number,
    organizationName?: string,
    enableUserPhone?: boolean,
    enableCustomLogin?: boolean,
    isUserDeleteDisabled?: boolean,
    logoUrl?: string,
    faviconUrl?: string,
    privacyUrl?: string,
    helpUrl?: string,
    termsUrl?: string,
    copyrightMessage?: string,
    avatarBucket?: string,
    systemEmailAccount?: string,
    otpExpiry?: number,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + UPDATE_SETTINGS_ENDPOINT;
        if (!organizationName) organizationName = undefined;

        return this.http.post(
          requestUrl,
          {
            issuerUrl,
            disableSignup,
            infrastructureConsoleClientId,
            enableChoosingAccount,
            refreshTokenExpiresInDays,
            authCodeExpiresInMinutes,
            organizationName,
            enableUserPhone,
            enableCustomLogin,
            isUserDeleteDisabled,
            logoUrl,
            faviconUrl,
            privacyUrl,
            helpUrl,
            termsUrl,
            copyrightMessage,
            avatarBucket,
            systemEmailAccount,
            otpExpiry,
          },
          { headers },
        );
      }),
    );
  }

  getClientUpdate(
    appURL: string,
    authServerURL: string,
    clientId: string,
    clientSecret: string,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + UPDATE_SETTINGS_ENDPOINT;
        return this.http
          .get<OpenIDConfiguration>(
            authServerURL + OPENID_CONFIGURATON_ENDPOINT,
          )
          .pipe(
            switchMap(response => {
              return this.http.post(
                requestUrl,
                {
                  appURL,
                  adminUrl: config.adminUrl,
                  authServerURL,
                  clientId,
                  clientSecret,
                  authorizationURL: response.authorization_endpoint,
                  callbackURLs: [
                    config.adminUrl + environment.routes.OAUTH2_CALLBACK,
                  ],
                  introspectionURL: response.introspection_endpoint,
                  profileURL: response.userinfo_endpoint,
                  revocationURL: response.revocation_endpoint,
                  tokenURL: response.token_endpoint,
                },
                { headers },
              );
            }),
          );
      }),
    );
  }

  getEmailAccounts() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + LIST_EMAILS_ENDPOINT;
        return this.http.get<ListResponse>(requestUrl, { headers });
      }),
      map(data => data.docs),
    );
  }

  getSavedEmailAccount<T>() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + GET_SETTINGS_ENDPOINT;
        return this.http.get<T>(requestUrl, { headers });
      }),
    );
  }

  getBucketOptions() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + LIST_STORAGE_ENDPOINT;
        return this.http.get<ListResponse>(requestUrl, { headers });
      }),
      map(data => data.docs),
    );
  }

  deleteBearerTokens() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + DELETE_BEARER_TOKENS_ENDPOINT;
        return this.http.post(requestUrl, {}, { headers });
      }),
    );
  }

  deleteUserSessions() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestUrl = config.issuerUrl + DELETE_USER_SESSIONS_ENDPOINT;
        return this.http.post(requestUrl, {}, { headers });
      }),
    );
  }

  logout() {
    return this.token.config.subscribe({
      next: config => {
        const logoutUrl =
          config.issuerUrl + LOGOUT_URL + '?redirect=' + config.adminUrl;
        this.store.clear();
        this.token.logOut();
        window.location.href = logoutUrl;
      },
    });
  }
}
