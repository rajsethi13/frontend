export interface IAuthSettings {
  appURL?: string;
  authServerURL?: string;
  authorizationURL?: string;
  callbackURLs?: string[];
  clientId: string;
  clientSecret: string;
  communicationServerSystemEmailAccount?: string;
  introspectionURL?: string;
  profileURL?: string;
  revocationURL?: string;
  service?: string;
  tokenURL?: string;
  uuid?: string;
  serviceName?: string;
  cloudStorageSettings?: string;
  issuerUrl?: string;
  infrastructureConsoleClientId?: string;
  communicationServerClientId?: string;
  disableSignup?: boolean;
  enableChoosingAccount?: boolean;
  refreshTokenExpiresInDays?: number;
  authCodeExpiresInMinutes?: number;
  enableUserPhone?: boolean;
  enableCustomLogin?: boolean;
  isUserDeleteDisabled?: boolean;
  avatarBucket?: string;
  systemEmailAccount?: string;
  logoUrl?: string;
  termsUrl?: string;
  privacyUrl?: string;
  helpUrl?: string;
  faviconUrl?: string;
  copyrightMessage?: string;
  organizationName?: string;
  otpExpiry?: number;
}
