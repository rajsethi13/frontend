import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthSettingsService } from './auth-settings.service';
import {
  CLOSE,
  UPDATE_SUCCESSFUL,
  DELETING,
  UNDO,
} from '../../constants/messages';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DURATION, UNDO_DURATION, THIRTY } from '../../constants/common';
import { IAuthSettings } from './auth-settings.interface';

@Component({
  selector: 'app-auth-settings',
  templateUrl: './auth-settings.component.html',
  styleUrls: ['./auth-settings.component.css'],
})
export class AuthSettingsComponent implements OnInit {
  issuerUrl: string;
  identityProviderClientId: string;
  infrastructureConsoleClientId: string;
  communicationServerClientId: string;
  clientList: any[];
  storageList: any[];
  appURL: string;
  clientId: string;
  clientSecret: string;
  avatarBucket: string;
  systemEmailAccount: string;
  emailAccounts: any[];
  cloudStorageList: { uuid: string; name: string }[] = [];
  disableSignup: boolean;
  flagDeleteUserSessions: boolean = false;
  flagDeleteBearerTokens: boolean = false;
  disableDeleteSessions: boolean = false;
  disableDeleteTokens: boolean = false;
  enableChoosingAccount: boolean;
  enableCustomLogin: boolean;
  refreshTokenExpiresInDays: number = THIRTY;
  authCodeExpiresInMinutes: number = THIRTY;
  organizationName: string;
  enableUserPhone: boolean;
  isUserDeleteDisabled: boolean;
  otpExpiry: number;

  authSettingsForm = new FormGroup({
    issuerUrl: new FormControl(),
    disableSignup: new FormControl(),
    infrastructureConsoleClientId: new FormControl(),
    systemEmailAccount: new FormControl(),
    enableChoosingAccount: new FormControl(),
    refreshTokenExpiresInDays: new FormControl(),
    authCodeExpiresInMinutes: new FormControl(),
    organizationName: new FormControl(),
    enableUserPhone: new FormControl(),
    enableCustomLogin: new FormControl(),
    isUserDeleteDisabled: new FormControl(),
    logoURL: new FormControl(),
    faviconURL: new FormControl(),
    privacyURL: new FormControl(),
    helpURL: new FormControl(),
    termsURL: new FormControl(),
    copyrightMessage: new FormControl(),
    avatarBucket: new FormControl(),
    otpExpiry: new FormControl(),
  });

  constructor(
    private settingsService: AuthSettingsService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.settingsService.getSettings().subscribe({
      next: (response: IAuthSettings) => {
        this.issuerUrl = response.issuerUrl;
        this.communicationServerClientId = response.communicationServerClientId;
        this.disableSignup = response.disableSignup;
        this.enableChoosingAccount = response.enableChoosingAccount;
        this.refreshTokenExpiresInDays = response.refreshTokenExpiresInDays;
        this.authCodeExpiresInMinutes = response.authCodeExpiresInMinutes;
        this.enableUserPhone = response.enableUserPhone;
        this.isUserDeleteDisabled = response.isUserDeleteDisabled;
        this.systemEmailAccount = response.systemEmailAccount;
        this.avatarBucket = response.avatarBucket;
        this.enableCustomLogin = response.enableCustomLogin;
        this.otpExpiry = response.otpExpiry;
        this.infrastructureConsoleClientId =
          response.infrastructureConsoleClientId;
        this.populateForm(response);
      },
      error: error => {},
    });

    this.settingsService.getClientList().subscribe({
      next: (response: any[]) => {
        this.clientList = response;
      },
      error: error => {},
    });

    this.settingsService.getStorageList().subscribe({
      next: (response: any[]) => {
        this.storageList = response;
      },
      error: error => {},
    });

    this.settingsService.getEmailAccounts().subscribe({
      next: (response: any[]) => {
        this.emailAccounts = response;
      },
      error: error => {},
    });
  }

  populateForm(response: IAuthSettings) {
    this.authSettingsForm.controls.issuerUrl.setValue(response.issuerUrl);
    this.authSettingsForm.controls.disableSignup.setValue(
      response.disableSignup,
    );
    this.authSettingsForm.controls.infrastructureConsoleClientId.setValue(
      response.infrastructureConsoleClientId,
    );
    this.authSettingsForm.controls.enableChoosingAccount.setValue(
      response.enableChoosingAccount,
    );
    this.authSettingsForm.controls.refreshTokenExpiresInDays.setValue(
      response.refreshTokenExpiresInDays,
    );
    this.authSettingsForm.controls.authCodeExpiresInMinutes.setValue(
      response.authCodeExpiresInMinutes,
    );
    this.authSettingsForm.controls.organizationName.setValue(
      response.organizationName,
    );
    this.authSettingsForm.controls.enableUserPhone.setValue(
      response.enableUserPhone,
    );
    this.authSettingsForm.controls.enableCustomLogin.setValue(
      response.enableCustomLogin,
    );
    this.authSettingsForm.controls.isUserDeleteDisabled.setValue(
      response.isUserDeleteDisabled,
    );
    this.authSettingsForm.controls.avatarBucket.setValue(response.avatarBucket);
    this.authSettingsForm.controls.logoURL.setValue(response.logoUrl);
    this.authSettingsForm.controls.termsURL.setValue(response.termsUrl);
    this.authSettingsForm.controls.privacyURL.setValue(response.privacyUrl);
    this.authSettingsForm.controls.helpURL.setValue(response.helpUrl);
    this.authSettingsForm.controls.faviconURL.setValue(response.faviconUrl);
    this.authSettingsForm.controls.copyrightMessage.setValue(
      response.copyrightMessage,
    );
    this.authSettingsForm.controls.systemEmailAccount.setValue(
      response.systemEmailAccount,
    );
    this.authSettingsForm.controls.organizationName.disable();
    this.authSettingsForm.controls.otpExpiry.setValue(response.otpExpiry);
  }

  updateAuthSettings() {
    const logoURL =
      this.authSettingsForm.controls.logoURL.value === ''
        ? null
        : this.authSettingsForm.controls.logoURL.value;
    const faviconURL =
      this.authSettingsForm.controls.faviconURL.value === ''
        ? null
        : this.authSettingsForm.controls.faviconURL.value;
    const privacyURL =
      this.authSettingsForm.controls.privacyURL.value === ''
        ? null
        : this.authSettingsForm.controls.privacyURL.value;
    const helpURL =
      this.authSettingsForm.controls.helpURL.value === ''
        ? null
        : this.authSettingsForm.controls.helpURL.value;
    const termsURL =
      this.authSettingsForm.controls.termsURL.value === ''
        ? null
        : this.authSettingsForm.controls.termsURL.value;
    this.settingsService
      .update(
        this.authSettingsForm.controls.issuerUrl.value,
        this.authSettingsForm.controls.disableSignup.value,
        this.authSettingsForm.controls.infrastructureConsoleClientId.value,
        this.authSettingsForm.controls.enableChoosingAccount.value,
        this.authSettingsForm.controls.refreshTokenExpiresInDays.value,
        this.authSettingsForm.controls.authCodeExpiresInMinutes.value,
        this.authSettingsForm.controls.organizationName.value,
        this.authSettingsForm.controls.enableUserPhone.value,
        this.authSettingsForm.controls.enableCustomLogin.value,
        this.authSettingsForm.controls.isUserDeleteDisabled.value,
        logoURL,
        faviconURL,
        privacyURL,
        helpURL,
        termsURL,
        this.authSettingsForm.controls.copyrightMessage.value,
        this.authSettingsForm.controls.avatarBucket.value,
        this.authSettingsForm.controls.systemEmailAccount.value,
        Number(this.authSettingsForm.controls.otpExpiry.value || 5),
      )
      .subscribe({
        next: response => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
        },
        error: error => {},
      });
  }

  deleteUserSessions() {
    this.flagDeleteUserSessions = true;
    this.disableDeleteSessions = true;
    this.disableDeleteTokens = true;
    const snackBar = this.snackBar.open(DELETING, UNDO, {
      duration: UNDO_DURATION,
    });

    snackBar.afterDismissed().subscribe({
      next: dismissed => {
        if (this.flagDeleteUserSessions) {
          this.settingsService.deleteUserSessions().subscribe({
            next: deleted => {
              this.logout();
            },
            error: error => {},
          });
        }
      },
      error: error => {},
    });

    snackBar.onAction().subscribe({
      next: success => {
        this.flagDeleteUserSessions = false;
        this.disableDeleteSessions = false;
        this.disableDeleteTokens = false;
      },
      error: error => {},
    });
  }

  deleteBearerTokens() {
    this.flagDeleteUserSessions = true;
    this.disableDeleteSessions = true;
    this.disableDeleteTokens = true;
    const snackBar = this.snackBar.open(DELETING, UNDO, {
      duration: UNDO_DURATION,
    });

    snackBar.afterDismissed().subscribe({
      next: dismissed => {
        if (this.flagDeleteUserSessions) {
          this.settingsService.deleteBearerTokens().subscribe({
            next: deleted => {
              this.logout();
            },
            error: error => {},
          });
        }
      },
      error: error => {},
    });

    snackBar.onAction().subscribe({
      next: success => {
        this.flagDeleteUserSessions = false;
        this.disableDeleteSessions = false;
        this.disableDeleteTokens = false;
      },
      error: error => {},
    });
  }

  toggleOrgName() {
    if (this.authSettingsForm.controls.organizationName.disabled) {
      this.authSettingsForm.controls.organizationName.enable();
    }
  }

  logout() {
    this.settingsService.logout();
  }

  kebabToTitleCase(string: string) {
    return string
      .split('-')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }
}
