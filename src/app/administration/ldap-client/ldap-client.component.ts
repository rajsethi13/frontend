import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { LDAPClientService } from './ldap-client.service';
import { NEW_ID, DURATION } from '../../constants/common';
import {
  LDAP_CLIENT_ERROR,
  LDAP_CLIENT_UPDATED,
  LDAP_CLIENT_CREATED,
  CLOSE,
  UPDATE_ERROR,
  DELETE_ERROR,
} from '../../constants/messages';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { AuthSettingsService } from '../auth-settings/auth-settings.service';

export const LDAP_CLIENT_LIST_ROUTE = ['admin', 'list', 'ldap_client'];

@Component({
  selector: 'app-ldap-client',
  templateUrl: './ldap-client.component.html',
  styleUrls: ['./ldap-client.component.css'],
})
export class LDAPClientComponent implements OnInit {
  uuid: string;
  name: string;
  hideAdminPassword: boolean = true;
  attributeForm = new FormArray([]);
  ldapClientForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    url: new FormControl(),
    adminDn: new FormControl(),
    adminPassword: new FormControl(),
    userSearchBase: new FormControl(),
    usernameAttribute: new FormControl(),
    emailAttribute: new FormControl(),
    fullNameAttribute: new FormControl(),
    phoneAttribute: new FormControl(),
    clientId: new FormControl(),
    scope: new FormControl(),
    attributes: this.attributeForm,
  });
  new = NEW_ID;
  clientList: any[];
  scopeList: any[];

  constructor(
    private ldapClientService: LDAPClientService,
    private settings: AuthSettingsService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {
    this.uuid = this.route.snapshot.params.id;
  }

  ngOnInit() {
    if (this.uuid && this.uuid !== this.new) {
      this.subscribeGetLDAPClients(this.uuid);
    } else if (this.uuid === NEW_ID) {
      this.uuid = undefined;
    }
    this.settings.getClientList().subscribe({
      next: (response: any[]) => {
        this.clientList = response;
      },
      error: error => {},
    });
    this.ldapClientService.getScopes().subscribe({
      next: response => {
        this.scopeList = response;
      },
      error: error => {},
    });
  }

  subscribeGetLDAPClients(uuid: string) {
    this.ldapClientService.getLDAPClient(uuid).subscribe({
      next: response => {
        if (response) {
          this.populateForm(response);
        }
      },
    });
  }

  populateForm(ldapClient) {
    this.name = ldapClient.name;
    (ldapClient?.attributes || []).forEach(attribute => {
      this.addAttribute(attribute);
    });
    this.ldapClientForm.controls.name.setValue(ldapClient.name);
    this.ldapClientForm.controls.description.setValue(ldapClient.description);
    this.ldapClientForm.controls.clientId.setValue(ldapClient.clientId);
    this.ldapClientForm.controls.adminPassword.setValue(
      ldapClient.adminPassword,
    );
    this.ldapClientForm.controls.adminDn.setValue(ldapClient.adminDn);
    this.ldapClientForm.controls.userSearchBase.setValue(
      ldapClient.userSearchBase,
    );
    this.ldapClientForm.controls.usernameAttribute.setValue(
      ldapClient.usernameAttribute,
    );
    this.ldapClientForm.controls.emailAttribute.setValue(
      ldapClient.emailAttribute,
    );
    this.ldapClientForm.controls.fullNameAttribute.setValue(
      ldapClient.fullNameAttribute,
    );
    this.ldapClientForm.controls.phoneAttribute.setValue(
      ldapClient.phoneAttribute,
    );
    this.ldapClientForm.controls.url.setValue(ldapClient.url);
    this.ldapClientForm.controls.scope.setValue(ldapClient.scope);
  }

  createLDAPClient() {
    this.ldapClientService
      .createLDAPClient(
        this.ldapClientForm.controls.name.value,
        this.ldapClientForm.controls.description.value,
        this.ldapClientForm.controls.url.value,
        this.ldapClientForm.controls.adminDn.value,
        this.ldapClientForm.controls.adminPassword.value,
        this.ldapClientForm.controls.userSearchBase.value,
        this.ldapClientForm.controls.usernameAttribute.value,
        this.ldapClientForm.controls.emailAttribute.value,
        this.ldapClientForm.controls.phoneAttribute.value,
        this.ldapClientForm.controls.fullNameAttribute.value,
        this.ldapClientForm.controls.clientId.value,
        this.ldapClientForm.controls.scope.value,
        this.getAttributes(),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(LDAP_CLIENT_CREATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(LDAP_CLIENT_LIST_ROUTE);
        },
        error: error =>
          this.snackBar.open(LDAP_CLIENT_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  updateLDAPClient() {
    this.ldapClientService
      .updateLDAPClient(
        this.uuid,
        this.ldapClientForm.controls.name.value,
        this.ldapClientForm.controls.description.value,
        this.ldapClientForm.controls.url.value,
        this.ldapClientForm.controls.adminDn.value,
        this.ldapClientForm.controls.adminPassword.value,
        this.ldapClientForm.controls.userSearchBase.value,
        this.ldapClientForm.controls.usernameAttribute.value,
        this.ldapClientForm.controls.emailAttribute.value,
        this.ldapClientForm.controls.phoneAttribute.value,
        this.ldapClientForm.controls.fullNameAttribute.value,
        this.ldapClientForm.controls.clientId.value,
        this.ldapClientForm.controls.scope.value,
        this.getAttributes(),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(LDAP_CLIENT_UPDATED, CLOSE, {
            duration: DURATION,
          });
          this.router.navigate(LDAP_CLIENT_LIST_ROUTE);
        },
        error: error =>
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ldapClientService.deleteLDAPClient(this.uuid).subscribe({
          next: res => {
            this.router.navigate(LDAP_CLIENT_LIST_ROUTE);
          },
          error: error => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        });
      }
    });
  }

  addAttribute(attribute?: string) {
    this.attributeForm.push(
      new FormGroup({ attribute: new FormControl(attribute) }),
    );
  }

  removeAttribute(formGroupID: number) {
    this.attributeForm.removeAt(formGroupID);
  }

  getAttributes(): string[] {
    const attributes: string[] = [];
    for (const control of this.attributeForm.controls) {
      attributes.push(control.value.attribute);
    }
    return attributes;
  }
}
