import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_LDAP_CLIENT_ENDPOINT,
  DELETE_LDAP_CLIENT_ENDPOINT,
  FIND_SCOPE_ENDPOINT,
  GET_LDAP_CLIENT_ENDPOINT,
  UDPATE_LDAP_CLIENT_ENDPOINT,
} from '../../constants/url-paths';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LDAPClientService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getLDAPClient(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_LDAP_CLIENT_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createLDAPClient(
    name: string,
    description: string,
    url: string,
    adminDn: string,
    adminPassword: string,
    userSearchBase: string,
    usernameAttribute: string,
    emailAttribute: string,
    phoneAttribute: string,
    fullNameAttribute: string,
    clientId: string,
    scope: string,
    attributes: string[],
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestURL = config.issuerUrl + `${CREATE_LDAP_CLIENT_ENDPOINT}`;
        return this.http.post(
          requestURL,
          {
            name,
            description,
            url,
            adminDn,
            adminPassword,
            userSearchBase,
            usernameAttribute,
            emailAttribute,
            phoneAttribute,
            fullNameAttribute,
            clientId,
            scope,
            attributes,
          },
          { headers },
        );
      }),
    );
  }

  updateLDAPClient(
    uuid: string,
    name: string,
    description: string,
    url: string,
    adminDn: string,
    adminPassword: string,
    userSearchBase: string,
    usernameAttribute: string,
    emailAttribute: string,
    phoneAttribute: string,
    fullNameAttribute: string,
    clientId: string,
    scope: string,
    attributes: string[],
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const requestURL =
          config.issuerUrl + `${UDPATE_LDAP_CLIENT_ENDPOINT}/${uuid}`;
        return this.http.post(
          requestURL,
          {
            name,
            description,
            url,
            adminDn,
            adminPassword,
            userSearchBase,
            usernameAttribute,
            emailAttribute,
            phoneAttribute,
            fullNameAttribute,
            clientId,
            scope,
            attributes,
          },
          { headers },
        );
      }),
    );
  }

  deleteLDAPClient(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${DELETE_LDAP_CLIENT_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }

  getScopes() {
    return forkJoin({
      headers: this.token.getHeaders(),
      config: this.token.config,
    }).pipe(
      switchMap(({ headers, config }) => {
        const url = `${config.issuerUrl}${FIND_SCOPE_ENDPOINT}`;
        return this.http.get<any[]>(url, { headers });
      }),
    );
  }
}
