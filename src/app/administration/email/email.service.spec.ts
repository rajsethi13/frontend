import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { EmailService } from './email.service';

describe('EmailService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: EmailService = TestBed.inject(EmailService);
    expect(service).toBeTruthy();
  });
});
