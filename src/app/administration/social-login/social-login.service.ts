import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, switchMap } from 'rxjs/operators';
import { TokenService } from '../../auth/token/token.service';
import {
  CREATE_SOCIAL_LOGIN_ENDPOINT,
  DELETE_SOCIAL_LOGIN_ENDPOINT,
  GET_SOCIAL_LOGIN_ENDPOINT,
  SOCIAL_LOGIN_CALLBACK_ENDPOINT,
  UPDATE_SOCIAL_LOGIN_ENDPOINT,
} from '../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class SocialLoginService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getSocialLogin(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_SOCIAL_LOGIN_ENDPOINT}/${uuid}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createSocialLogin(
    name: string,
    description: string,
    clientId: string,
    clientSecret: string,
    authorizationURL: string,
    tokenURL: string,
    introspectionURL: string,
    baseURL: string,
    profileURL: string,
    revocationURL: string,
    scope: string[],
    clientSecretToTokenEndpoint: boolean,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${CREATE_SOCIAL_LOGIN_ENDPOINT}`;
        return this.http.post(
          url,
          {
            name,
            description,
            clientId,
            clientSecret,
            authorizationURL,
            tokenURL,
            introspectionURL,
            baseURL,
            profileURL,
            revocationURL,
            scope,
            clientSecretToTokenEndpoint,
          },
          { headers },
        );
      }),
    );
  }

  updateSocialLogin(
    uuid: string,
    name: string,
    description: string,
    clientId: string,
    clientSecret: string,
    authorizationURL: string,
    tokenURL: string,
    introspectionURL: string,
    baseURL: string,
    profileURL: string,
    revocationURL: string,
    scope: string[],
    clientSecretToTokenEndpoint: boolean,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl + `${UPDATE_SOCIAL_LOGIN_ENDPOINT}/${uuid}`;
        return this.http.post(
          url,
          {
            name,
            description,
            clientId,
            clientSecret,
            authorizationURL,
            tokenURL,
            introspectionURL,
            baseURL,
            profileURL,
            revocationURL,
            scope,
            clientSecretToTokenEndpoint,
          },
          { headers },
        );
      }),
    );
  }

  generateRedirectURL(uuid: string) {
    return this.token.config.pipe(
      map(
        config =>
          config.issuerUrl + SOCIAL_LOGIN_CALLBACK_ENDPOINT + '/' + uuid,
      ),
    );
  }

  deleteSocialLogin(uuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url =
          config.issuerUrl + `${DELETE_SOCIAL_LOGIN_ENDPOINT}/${uuid}`;
        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
