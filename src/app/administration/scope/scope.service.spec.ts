import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ScopeService } from './scope.service';

describe('ScopeService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [],
    }),
  );

  it('should be created', () => {
    const service: ScopeService = TestBed.get(ScopeService);
    expect(service).toBeTruthy();
  });
});
