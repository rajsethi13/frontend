import { Injectable } from '@angular/core';
import {
  CLIENT_ID,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  LOGIN_URL,
  USER_UUID,
} from '../../constants/storage';
import { StorageService } from '../../auth/storage/storage.service';

@Injectable()
export class NavigationService {
  constructor(private readonly store: StorageService) {}

  clearInfoStorage() {
    this.store.removeItem(CLIENT_ID);
    this.store.removeItem(REDIRECT_URI);
    this.store.removeItem(SILENT_REFRESH_REDIRECT_URI);
    this.store.removeItem(LOGIN_URL);
    this.store.removeItem(USER_UUID);
  }
}
