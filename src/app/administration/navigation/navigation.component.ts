import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { APP_URL, USER_UUID, COMMUNICATION } from '../../constants/storage';
import { LOGOUT_URL } from '../../constants/url-paths';
import { TokenService } from '../../auth/token/token.service';
import { LOGGED_IN } from '../../auth/token/constants';
import { SET_ITEM, StorageService } from '../../auth/storage/storage.service';
import { FORM, LIST, NEW_ID } from '../../constants/common';
import { ADMINISTRATOR } from '../../constants/app-constants';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  tokenIsValid: boolean;
  isAdmin: boolean = false;
  route: string;
  loggedIn: boolean;
  isCommunicationEnabled: boolean;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private token: TokenService,
    private store: StorageService,
  ) {}

  ngOnInit(): void {
    this.token.loadProfile().subscribe({
      next: profile => {
        this.isAdmin = profile?.roles.includes(ADMINISTRATOR);
      },
    });
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe({
        next: (route: NavigationEnd) => {
          this.route = route.url;
        },
      });
    if (!this.route) {
      this.route = this.router.url;
    }
    const loggedIn = this.store.getItem(LOGGED_IN);
    if (loggedIn === 'true') {
      this.tokenIsValid = true;
      this.loggedIn = true;
    }

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          if (res.value?.value === 'true') {
            this.loggedIn = true;
            this.tokenIsValid = true;
          }
        }
      },
      error: error => {},
    });

    try {
      this.isCommunicationEnabled = JSON.parse(
        this.store.getItem(COMMUNICATION),
      );
    } catch (error) {
      this.isCommunicationEnabled = false;
    }
  }

  login() {
    this.token.logIn();
  }

  chooseAccount() {
    const appURL = this.store.getItem(APP_URL);
    window.open(appURL, '_blank', 'noreferrer=true');
  }

  logoutCurrentUser() {
    this.token.config.subscribe({
      next: config => {
        const userUUID = this.store.getItem(USER_UUID);
        this.store.clear();
        window.location.href =
          config.issuerUrl +
          LOGOUT_URL +
          '/' +
          userUUID +
          '?redirect=' +
          encodeURIComponent(location.href);
      },
    });
  }

  addModel() {
    // TODO: make it better in UI/UX
    const routeArray = this.route.split('/');
    routeArray.slice(-1);
    if (!routeArray.includes(NEW_ID) && routeArray.includes(FORM)) {
      routeArray.pop();
      routeArray.push(NEW_ID);
    }
    if (routeArray.includes(LIST)) {
      routeArray.push(NEW_ID);
    }
    routeArray.filter(n => n);
    const route = routeArray.map(component => {
      if (component === LIST) {
        return FORM;
      }
      return component;
    });
    this.router
      .navigateByUrl('admin', { skipLocationChange: true })
      .then(() => this.router.navigate(route));
  }
}
