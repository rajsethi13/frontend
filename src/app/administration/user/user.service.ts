import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { UserUpdate } from './user-update.interface';
import { TokenService } from '../../auth/token/token.service';
import {
  FIND_ROLE_ENDPOINT,
  USER_DELETE_ENDPOINT,
  USER_DISABLE_PASSWORDLESS_ENDPOINT,
  USER_ENABLE_PASSWORDLESS_ENDPOINT,
  UPDATE_USER_ENDPOINT,
  CREATE_USER_ENDPOINT,
  INFO_ENDPOINT,
  GET_USER_ENDPOINT,
} from '../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getUser(userID: string): Observable<any> {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${GET_USER_ENDPOINT}/${userID}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  verifyUser(userURL: string) {
    const url = `${userURL}/${INFO_ENDPOINT}`;
    return this.token.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  createUser(
    fullName: string,
    userEmail: string,
    userPhone: number,
    userPassword: string,
    userRole: string,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${CREATE_USER_ENDPOINT}`;
        const userData = {
          name: fullName,
          email: userEmail,
          phone: userPhone,
          password: userPassword,
          roles: userRole,
        };
        return this.http.post(url, userData, { headers });
      }),
    );
  }

  updateUser(
    uuid: string,
    fullName: string,
    roles: string,
    password?: string,
    disabled = false,
  ) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${UPDATE_USER_ENDPOINT}/${uuid}`;

        const userData: UserUpdate = {
          name: fullName,
          roles,
          disabled,
        };

        if (password) userData.password = password;
        return this.http.post(url, userData, { headers });
      }),
    );
  }

  enablePasswordLessLogin(userUuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + USER_ENABLE_PASSWORDLESS_ENDPOINT;
        return this.http.post(url, { userUuid }, { headers });
      }),
    );
  }

  disablePasswordLessLogin(userUuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + USER_DISABLE_PASSWORDLESS_ENDPOINT;

        return this.http.post(url, { userUuid }, { headers });
      }),
    );
  }

  getRoles() {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `/${FIND_ROLE_ENDPOINT}`;
        return this.http.get<string>(url, { headers });
      }),
    );
  }

  deleteUser(userUuid: string) {
    return this.token.getConfigAndHeaders().pipe(
      switchMap(({ config, headers }) => {
        const url = config.issuerUrl + `${USER_DELETE_ENDPOINT}/${userUuid}`;

        return this.http.post(url, undefined, { headers });
      }),
    );
  }
}
