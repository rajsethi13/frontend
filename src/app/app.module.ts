import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthServerMaterialModule } from './auth-server-material/auth-server-material.module';
import { AuthService } from './auth/auth.service';
import { AVAILABLE_TRANSLATIONS } from './constants/app-strings';
import { CallbackPage } from './auth/callback/callback.page';
import { BrandInfoService } from './common/brand-info/brand-info.service';
import { TokenService } from './auth/token/token.service';
import { StorageService } from './auth/storage/storage.service';
import { AdministrationModule } from './administration/administration.module';
import { AuthenticationModule } from './authentication/authentication.module';

let lang = navigator.language;
if (!AVAILABLE_TRANSLATIONS.includes(lang)) {
  lang = 'en-US';
}

@NgModule({
  declarations: [AppComponent, CallbackPage],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AuthServerMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AdministrationModule,
    AuthenticationModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: lang },
    AuthService,
    BrandInfoService,
    TokenService,
    StorageService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
