import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EMPTY, Observable, Subject, from, throwError } from 'rxjs';
import {
  map,
  delay,
  catchError,
  concatMap,
  switchMap,
  takeUntil,
} from 'rxjs/operators';
import { get } from '@github/webauthn-json';
import { environment } from '../../environments/environment';
import { TokenService } from './token/token.service';

interface InfoResponse {
  session?: false;
  message?: any;
}

export interface OpenIDConfigurationeResponse {
  issuer: string;
  jwks_uri: string;
  authorization_endpoint: string;
  token_endpoint: string;
  userinfo_endpoint: string;
  revocation_endpoint: string;
  introspection_endpoint: string;
  response_types_supported: string[];
  subject_types_supported: string[];
  id_token_signing_alg_values_supported: string[];
}

@Injectable()
export class AuthService {
  public redirectTo;
  public isAuth: boolean;
  public issuerUrl: string;

  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  getConfig() {
    return this.token.config;
  }

  isAuthenticated(router?: Router): Observable<boolean> {
    return this.token.config.pipe(
      map((r: InfoResponse) => {
        if (r.session) {
          return r.session;
        } else if (router) {
          router.navigate(['login']);
        }
        return false;
      }),
    );
  }

  logIn(username: string, password: string, code?: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(config.issuerUrl + environment.routes.LOGIN, {
          username,
          password,
          code,
          redirect: this.redirectTo,
        });
      }),
    );
  }

  passwordLessLogin(username: string, code: string) {
    return this.token.config.pipe(
      switchMap(config => {
        return this.http.post(
          config.issuerUrl + environment.routes.LOGIN_PASSWORDLESS,
          {
            username,
            code,
            redirect: this.redirectTo,
          },
        );
      }),
    );
  }

  signUp(
    communicationEnabled: boolean,
    name: string,
    email: string,
    phone: string,
    password: string,
    redirect?: string,
  ) {
    return this.token.config.pipe(
      switchMap(config => {
        if (communicationEnabled) {
          return this.http.post(
            config.issuerUrl + environment.routes.SIGNUP_VIA_EMAIL,
            {
              name,
              email,
              redirect,
            },
          );
        }
        return this.http.post(config.issuerUrl + environment.routes.SIGNUP, {
          name,
          email,
          phone,
          password,
        });
      }),
    );
  }

  authorize(transactionId: string, consent: string = 'Allow') {
    return this.http.post(
      this.token.appBasePath + environment.routes.AUTHORIZE,
      {
        value: consent,
        transaction_id: transactionId,
      },
    );
  }

  verifyUser(username: string) {
    return this.http.post(
      this.token.appBasePath + environment.routes.CHECK_USER,
      {
        username,
      },
    );
  }

  verifyPassword(username, password) {
    return this.http.post(
      this.token.appBasePath + environment.routes.CHECK_PASSWORD,
      {
        username,
        password,
      },
    );
  }

  getSocialLogins() {
    return this.http.get(
      this.token.appBasePath + environment.routes.LIST_SOCIAL_LOGINS,
    );
  }

  getLDAPClients() {
    return this.http.get<{ name: string; uuid: string; enabled: boolean }[]>(
      this.token.appBasePath + environment.routes.LIST_LDAP_CLIENTS,
    );
  }

  forgotPassword(emailOrPhone: string) {
    return this.http.post(
      this.token.appBasePath + environment.routes.FORGOT_PASSWORD,
      { emailOrPhone },
    );
  }

  sendOTP(emailOrPhone: string) {
    return this.http
      .post(this.token.appBasePath + environment.routes.SEND_LOGIN_OTP, {
        emailOrPhone,
      })
      .pipe(delay(10000));
  }

  getSessionUsers() {
    return this.http.get(
      this.token.appBasePath + environment.routes.LIST_SESSION_USERS,
    );
  }

  selectUser<T>(uuid) {
    return this.http.post<T>(
      this.token.appBasePath + environment.routes.CHOOSE_USER,
      { uuid },
    );
  }

  webAuthnLogin(username: string, redirect?: string) {
    return this.http
      .post<any>(
        this.token.appBasePath + environment.routes.WEBAUTHN_LOGIN,
        { username },
        {
          headers: {
            'content-type': 'Application/Json',
          },
        },
      )
      .pipe(
        switchMap(challenge => {
          return from(get({ publicKey: challenge }));
        }),
        switchMap(credentials => {
          let params;
          if (redirect) params = { redirect };
          return this.http.post<{ redirect: string; loggedIn: boolean }>(
            this.token.appBasePath +
              environment.routes.WEBAUTHN_LOGIN_CHALLENGE,
            credentials,
            {
              headers: {
                'content-type': 'application/Json',
              },
              params,
            },
          );
        }),
      );
  }

  signUpViaPhone(name: string, unverifiedPhone: string) {
    return this.http.post(
      this.token.appBasePath + environment.routes.SIGNUP_VIA_PHONE,
      {
        name,
        unverifiedPhone,
      },
    );
  }

  verifySignupPhone(unverifiedPhone: string, otp: string) {
    return this.http.post(
      this.token.appBasePath + environment.routes.VERIFY_PHONE_SIGNUP,
      {
        otp,
        unverifiedPhone,
      },
    );
  }

  logInWithLDAP(
    clients: { name: string; uuid: string }[],
    username: string,
    password: string,
  ) {
    const last = clients.slice(-1).pop();
    const stopSignal$ = new Subject();

    return from(clients).pipe(
      concatMap(client => {
        return this.http
          .post(
            this.token.appBasePath +
              environment.routes.LDAP_LOGIN +
              '/' +
              client.uuid,
            { username, password, redirect: this.redirectTo },
            { observe: 'response' },
          )
          .pipe(
            map(response => {
              if (response.status === 200) {
              }
              return response.body;
            }),
            catchError(error => {
              if (last?.uuid === client.uuid) {
                return throwError(() => error);
              }
              return EMPTY;
            }),
            takeUntil(stopSignal$),
          );
      }),
    );
  }

  loginViaKerberos(realmUuid: string, redirect: string) {
    const options: { params?: { [param: string]: string } } = {};
    if (redirect) {
      options.params = { redirect };
    }
    return this.http.post(
      this.token.appBasePath +
        environment.routes.KERBEROS_LOGIN +
        '/' +
        realmUuid,
      undefined,
      options,
    );
  }
}
