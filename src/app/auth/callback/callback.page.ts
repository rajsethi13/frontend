import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from '../token/token.service';
import { SET_ITEM, StorageService } from '../storage/storage.service';
import { ACCESS_TOKEN } from '../token/constants';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.page.html',
  styleUrls: ['./callback.page.css'],
})
export class CallbackPage implements OnInit {
  constructor(
    private readonly store: StorageService,
    private readonly token: TokenService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === ACCESS_TOKEN) {
          if (res.value?.value !== 'undefined') {
            this.router.navigate(['admin', 'profile']);
          }
        }
      },
    });
    this.route.queryParams.subscribe(params => {
      if (params.code && params.state) {
        const url = location.href;
        this.token.processCode(url);
      }
    });
  }
}
