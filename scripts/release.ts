/* eslint no-console: 0 */

import { exec } from 'child_process';

const asyncExec = command => {
  return new Promise((resolve, reject) => {
    exec(command, (err, stdout, stderr) => {
      if (err) {
        return reject({ err, stderr });
      }
      return resolve(stdout);
    });
  });
};

function main() {
  const releaseType = process.argv.slice(2).pop();
  if (!['major', 'minor', 'patch'].includes(releaseType)) {
    console.error('Invalid release');
    process.exit(1);
  }
  console.log('Set blank git tag prefix');
  asyncExec('yarn config set version-tag-prefix ""')
    .then(out => {
      console.log(out);
      console.log('Set git commit message');
      return asyncExec('yarn config set version-git-message "Publish %s"');
    })
    .then(out => {
      console.log(out);
      console.log('Release');
      return asyncExec(`yarn version --${releaseType}`);
    })
    .then(out => {
      console.log(out);
      console.log('Reset git tag prefix');
      return asyncExec('yarn config set version-tag-prefix "v"');
    })
    .then(out => {
      console.log(out);
      console.log('Reset git commit message');
      return asyncExec('yarn config set version-git-message "v%s"');
    })
    .then(out => {
      console.log(out);
      console.log('Git Push');
      return asyncExec('git push --follow-tags');
    })
    .then(console.log)
    .catch(console.error);
}

main();
