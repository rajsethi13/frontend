# Development Setup

Easy to use VSCode devcontainer setup is available that configures backing services and tools required for development

Clone the repo, copy example configs and re-open in vscode devcontainer

```shell
git clone https://gitlab.com/castlecraft/rauth/frontend
code frontend
```

Once opened in devcontainer:

```shell
yarn
yarn start
# or
yarn start --ssl
```

Note: It needs R-Auth backend running on `http://0.0.0.0:3000` to make api calls.

# Lint, format and tests

Merge requests are tested for format, lint, unit and e2e tests.

To format code

```shell
yarn format
```

To lint code

```shell
yarn lint --fix
```

To run unit tests

```shell
yarn test
```

To run e2e tests

```shell
yarn e2e
```

# Maintenance Guide

## Releases

Maintainers can run the release script to bump version, commit publish message and tag and push commit and tags to remote.

```shell
yarn ts-node scripts/release.ts [major|minor|patch]
```

Note: If release type is anything other than major, minor or patch the script will throw error.

## Dependency Upgrades

Run following command to upgrade dependencies to latest version.

```shell
yarn upgrade-interactive --latest
```

Once the dependencies are upgraded run the commands from Lint, format and tests section to verify successful upgrade.
