describe('R-Auth Client', () => {
  it('Visits the initial project page', () => {
    cy.visit('/');
    cy.title().should('eq', 'R-Auth');
  });
});
