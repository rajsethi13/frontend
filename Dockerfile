FROM node:20-bullseye as builder

ARG BUILD_ARGS="--configuration production"
# Copy and build app
COPY . /home/rauth/frontend
WORKDIR /home/rauth
RUN cd frontend \
    && yarn \
    && yarn build ${BUILD_ARGS}

FROM nginxinc/nginx-unprivileged:1.27
COPY --from=0 --chown=nginx:nginx /home/rauth/frontend/dist/rauth-client /var/www/html
COPY --chown=nginx:nginx ./container/nginx-default.conf.template /etc/nginx/conf.d/default.conf.template
COPY --chown=nginx:nginx ./container/entrypoint.sh /usr/local/bin/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
