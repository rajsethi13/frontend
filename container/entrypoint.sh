#!/bin/bash

## Thanks
# https://serverfault.com/a/919212
##

set -e

export REWRITE_BASE_PATH="rewrite ${BASE_PATH}(.*) /\$1 break;"

if [[ -z "$API_HOST" ]]; then
    export API_HOST=localhost
fi

if [[ -z "$API_PORT" ]]; then
    export API_PORT=3000
fi

if [[ -z "$BASE_PATH" ]]; then
    export BASE_PATH="/"
    export REWRITE_BASE_PATH=""
fi

envsubst '${API_HOST}
    ${API_PORT}
    ${BASE_PATH}
    ${REWRITE_BASE_PATH}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"
